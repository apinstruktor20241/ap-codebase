# 📋 Overview of miniX assigments

- [📋 Overview of miniX assigments](#-overview-of-minix-assigments)
    - [📝MiniX\[1\]: RunMe and ReadMe (individual)](#minix1-runme-and-readme-individual)
    - [📝MiniX\[2\]: Geometric emoji (individual)](#minix2-geometric-emoji-individual)
    - [📝MiniX\[3\]: Designing a throbber (individual)](#minix3-designing-a-throbber-individual)
    - [📝MiniX\[4\]: Capture All (individual)](#minix4-capture-all-individual)
    - [📝MiniX\[5\]: A Generative Program (individual)](#minix5-a-generative-program-individual)
    - [📝MiniX\[6\]: Games with objects (individual)](#minix6-games-with-objects-individual)
    - [📝MiniX\[7\]: Working with APIs (Group)](#minix7-working-with-apis-group)
    - [📝MiniX\[8\]: Algorithmic flowcharts (Individual + Group)](#minix8-algorithmic-flowcharts-individual--group)
    - [📝MiniX\[9\]: Final Project (Group)](#minix9-final-project-group)

### 📝MiniX[1]: RunMe and ReadMe (individual)

**✍️ Brief [Here](https://aesthetic-programming.net/pages/1-getting-started.html#minix-runme-and-readme-28833) | due SUN mid-night**

**✍️ Peer feedback questions | due Wed mid-night:**

**🔥Describe🔥**:
- What do you see? What is the program about?
- How is it made? What (new/interesting) syntax were used?
- What does it make you think about or feel?
- Is it successful? does it explore the prompt in a compelling, interesting or unique way?

**🔥Reflect🔥**:
  - What are the differences between reading other people code and writing your own code. What can you learn from reading other works?
  - What's the similarities and differences between writing and coding?
  - What is programming and why you need to read and write code (a question relates to coding literacy - check Annette Vee's reading)?

### 📝MiniX[2]: Geometric emoji (individual)

**✍️ BRIEF [Here](https://aesthetic-programming.net/pages/2-variable-geometry.html#minix-geometric-emoji-08270) | due SUN mid-night**

**✍️ Peer feedback questions | due Wed mid-night:**

- **🔥Describe🔥**:
    - What do you see? What is the program about?
    - How is it made? What (new/interesting) syntax were used?
    - What does it make you think about or feel?
- **🔥Reflect🔥**:
    - Is it successful? does it explore the prompt in a compelling, interesting or unique way?
    - Which aspect do you like the most? How's the thinking/approach different from your own miniX?
    - Has the work addressed the aesthetics/politics of representation conceptually or visually? How?

### 📝MiniX[3]: Designing a throbber (individual)

**✍️ BRIEF [Here](https://aesthetic-programming.net/pages/3-infinite-loops.html#minix-designing-a-throbber-27866) | due NEXT SUN mid-night**

**✍️ Peer feedback questions | due Wed mid-night:**

- **🔥Describe🔥**:
    - What do you see? What is the program about?
    - How is it made? What (new/interesting) syntax were used?
    - What does it make you think about or feel?
- **🔥Reflect🔥**:
    - Is it successful? does it explore the prompt in a compelling, interesting or unique way?
    - Which aspect do you like the most? How's the thinking/approach different from your own miniX?
    - How does the work change or inform your understanding of (human and machine) time and temporality?
    - Can you relate and articulate the work with some of the concepts or quotes in the assigned readings?

### 📝MiniX[4]: Capture All (individual)

**✍️ BRIEF [Here](https://aesthetic-programming.net/pages/4-data-capture.html#minix-capture-all-15243) | due SUN mid-night**

**✍️ Peer feedback questions | due Wed mid-night:**

- **🔥Describe🔥**:
    - What do you see? What is the program about?
    - What have been captured and what data have ben captured?
    - Does it link to any of the assigned readings explicitly? If yes, how's the conceptual linkage/reflection about the work beyond just technical description and implementation?

- **🔥Reflect🔥**:
    - Is it successful? Which aspect do you like the most? How's the thinking/approach different from your own miniX?
    - Do you have any suggestion for this miniX? (both ReadMe and RunMe)
    - How does the work change or inform your understanding of data capture?

### 📝MiniX[5]: A Generative Program (individual)

**✍️ BRIEF [Here](https://aesthetic-programming.net/pages/5-auto-generator.html#minix-a-generative-program-26756) | due SUN mid-night**

**✍️ Peer feedback questions | due Wed mid-night:**

- **🔥Describe🔥**:
    - What do you see? and What are the new / interesting syntax that have been used?
    - What are the rules and how the rules are implemented? (It is not the whole program, but more specifically the rules that lead to generativity.)
    - How about the conceptual linkage/reflection about the work beyond just technical description and implementation? (e.g generativity, rule-based system, (un)predictability, control, etc)
    - Does it link some of the concepts to the cirriculum (AP/SS) explicitly?

- **🔥Reflect🔥**:
    - Do you like the design of the program, and why/why not?
    - How does the work help you to see and think about the idea of “auto-generator” (e.g. levels of control, autonomy, love and care via rules)?
    - How is the thought process different when compare with your own?


### 📝MiniX[6]: Games with objects (individual)

**✍️ 1. BRIEF [Here](https://aesthetic-programming.net/pages/6-object-abstraction.html#minix-games-with-objects-86292) | due SUN mid-night**

**✍️ Peer feedback questions | due Wed mid-night:**

- **🔥Describe🔥**:
    - What is the game about and how do you play it?
    - What are the properties and behaviors of objects?
    - What does the work express?
    - How about the conceptual linkage/reflection about the work beyond just technical description and implementation?

- **🔥Reflect🔥**:
    - Which part of the design you like the most and why?
    - How do objects interact with the world?
    - How do worldviews and ideologies built into objects' properties and behaviors?

### 📝MiniX[7]: Working with APIs (Group)

**✍️ 1. BRIEF [Here](https://aesthetic-programming.net/pages/8-queery-data.html) | due SUN mid-night**

**✍️ Peer feedback questions | due Wed mid-night:**

- **🔥Describe🔥**:
  - What is the program about? Which API has been used?
  - What are the data types retrieved through the API, and how are they processed?
  - How about the conceptual linkage/reflection about the work beyond just technical description and implementation?

- **🔥Reflect🔥**:
  - Is it successful? Which aspect do you like the most? How's the thinking/approach different from your own miniX?
  - How does the work reflect the way in which machines and humans communicate through APIs?

### 📝MiniX[8]: Algorithmic flowcharts (Individual + Group)

**✍️ 1. BRIEF [Here](https://aesthetic-programming.net/pages/9-algorithmic-procedures.html#minix-flowcharts-67471) | due SUN mid-night**
- Your own repository should point to your individual flowchart and the group one.

**✍️ NO PEER FEEDBACK is required**

### 📝MiniX[9]: Final Project (Group)

[SUBJECT TO CHANGE]

**✍️ 1. BRIEF [Here](https://aesthetic-programming.net/pages/10-machine-unlearning.html#minix-final-project-93685) | due 16 May**

**✍️ 1. Draft | due on 8 May (SUN)**
- The draft should be 3-4 pages (exclude images, references and notes) in the form of a **PDF** and post to the disucssion forum (Go to BrightSpace > Course Tools > Discussion > Final Project Draft Submission > Select your group > Start a new Thread)
  - PDF of your project
    - Include a reference list
    - Include any sketch that you may have
    - Give a title to your project and make a revised flowchart

**✍️ 2. Feedback | due NEXT Wed before class:**
- Prepare an oral feedback within 8 mins
  - Use of text (assigned readings) and concepts
  - Linkage to the theme of Aesthetic Programming and the curriculum of the course
  - The clarity of the project
  - What you like the most, and why?
  - What are the less successful elements? What is your suggestion?
  - Any other comments


[def]: #minix8-flowcharts-individual--group