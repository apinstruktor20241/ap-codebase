# Peer feedback questions

## miniX01

✏️ **Describe**:
- What do you see? Can you explain what the program is about?
- How was is made? What part of the code do you find the most exciting and why?

✨ **Reflect**:
- Is it successful? Does it explore the prompt in a compelling, interesting, or unique way?
- How is this approach/way of thinking different from your own miniX?
- What are the differences between reading other people’s code and writing your own code? What can you learn from reading others’ works?

---

## miniX02

✏️ **Describe**:
- What do you see? Can you explain what the program is about?
- How was is made? What part of the code do you find the most exciting and why

✨ **Reflect**:
- Is it successful? Does it explore the prompt in a compelling, interesting, or unique way?
- How is this approach/way of thinking different from your own miniX?
- Has the work addressed the aesthetics/politics of representation conceptually or visually? - How?

---

## miniX03

✏️ **Describe**:
- What is the program about?
- What syntaxes were used?
- What does the work express?
- Does it link to the assigned readings explicitly? Conceptual linkage / reflection beyond the technical


✨ **Reflect**:
- Do you resonate with the design of the program, and why?
- Which aspect(s) do you like the most?
- How would you interpret the work, and how’s the thinking different from your own miniX?
- Can you relate the work to some of the concepts/quotes in texts?

---

## miniX04

✏️ **Describe**:
- What is the program about?
- What syntaxes were used?
- What data have been captured?
- Does it link to any of the assigned readings explicitly? If yes, how?

✨ **Reflect**:
- Is it successful? Does it explore the prompt in a compelling, interesting, or unique way?
- Which aspect(s) do you like the most?
- How would you interpret the work, and how’s the thinking different from your own miniX?
- How does the work change or inform your understanding of data capture?

---

## miniX05

✏️ **Describe**:
  - What do you see? and What are the new / interesting syntax that have been used?
  - What are the rules and how the rules are implemented? (It is not the whole program, but more specifically the rules that lead to generativity.)
  - How about the conceptual linkage/reflection about the work beyond just technical description and implementation? (e.g generativity, rule-based system, (un)predictability, control, etc)
  - Does it link some of the concepts to the cirriculum (AP/SS) explicitly?

✨ **Reflect**:
  - Do you like the design of the program, and why/why not?
  - How does the work help you to see and think about the idea of “auto-generator” (e.g. levels of control, autonomy, love and care via rules)?
  - How is the thought process different when compare with your own?

---

## miniX06

✏️ **Describe**:
  - What is the game about and how do you play it?
  - What are the properties and behaviors of objects?
  - What does the work express?
  - How about the conceptual linkage/reflection about the work beyond just technical description and implementation?

✨ **Reflect**:
  - Which part of the design you like the most and why?
  - How do objects interact with the world?
  - How do worldviews and ideologies built into objects' properties and behaviors?

---

## miniX07

✏️ **Describe**:
- What is the program about? Which API has been used?
- What are the data types retrieved through the API, and how are they processed?
- How about the conceptual linkage/reflection about the work beyond just technical description and implementation?

✨ **Reflect**:
- Is it successful? Which aspect do you like the most? How's the thinking/approach different from your own miniX?
- How does the work reflect the way in which machines and humans communicate through APIs?

---

## miniX08

**✍️ NO PEER FEEDBACK is required**

---

## miniX09

  **💬 Prepare an oral feedback within 8 mins**:
  - Use of text (assigned readings) and concepts
  - Linkage to the theme of Aesthetic Programming and the curriculum of the course
  - The clarity of the project
  - What you like the most, and why?
  - What are the less successful elements? What is your suggestion?
  - Any other comments