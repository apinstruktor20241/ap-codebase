const baseMargin = 16;
let imageResponseUrl;
let apiUrl = 'https://api.openai.com/v1/images/generations';

let apiKey;
let data;
let updateApiKeyButton;
let userPrompt;
let imageReceived = false;
let imageElementCreated = false;

function setup() {
  createCanvas(512+ baseMargin * 2, 512 + baseMargin * 6);
  apiKeyInput = createInput();
  apiKeyInput.attribute('placeholder', 'API Key');
  apiKeyInput.position(baseMargin, baseMargin);
  apiKeyInput.size(200);
  
  updateApiKeyButton = createButton('Update');
  updateApiKeyButton.position(apiKeyInput.width + updateApiKeyButton.width / 2 + baseMargin / 2, baseMargin);
  updateApiKeyButton.mousePressed(() => {apiKey = apiKeyInput.value(); console.log("API Key has been updated!")});

  let promptInput = createInput();
  promptInput.attribute('placeholder', 'Prompt');
  promptInput.position(baseMargin, baseMargin * 3);
  promptInput.size(width - (baseMargin * 2.5) - updateApiKeyButton.width - baseMargin * 1.5);

  let sendPromptButton = createButton('Prompt');
  sendPromptButton.position(promptInput.width + sendPromptButton.width / 2 + baseMargin / 2, baseMargin * 3);
  sendPromptButton.mousePressed(() => { 
    userPrompt = promptInput.value(); 
    requestData = constructRequestData(userPrompt); 
    apiQuery(apiUrl, apiKey, requestData);
  });
};

function draw() {
  frameRate(1);
  background(255);
  
  push();
  stroke(124, 124, 124);
  rect(baseMargin, baseMargin * 5, width-baseMargin*2, height-baseMargin*6);
  pop();

    if (imageReceived && !imageElementCreated) {
        let img = createImg(imageResponseUrl, "Image gneerated by DALL-E 3");
        img.position(baseMargin, baseMargin * 5);
        img.size(512, 512);
        imageElementCreated = true;
    }

}

function apiQuery(apiUrl, apiKey, requestData) {
    console.log("Sending prompt...")
    fetch(apiUrl, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer '+ apiKey
      },
      body: JSON.stringify(requestData)
    })
   .then(response => response.json())
   .then(data => {
      console.log("Prompt sent successfully!")
      console.log(`Data received! Printing data...`)
      console.log(data)
      imageResponseUrl = data.data[0].url;
      return imageResponseUrl; // return the url for the next .then
    })
   .then(url => {
      console.log(`Image URL received: ${url}`);
      imageReceived = true;
    })
   .catch(error => {
      console.error(error);
    });
  }

function constructRequestData(prompt, model = "dall-e-3") {
  return {
    "model": model,
    "prompt": prompt,
    "n": 1,
    "size": "1024x1024"
  };
}