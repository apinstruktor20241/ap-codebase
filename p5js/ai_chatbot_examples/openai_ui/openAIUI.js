const baseMargin = 16;
let textResponse = "Waiting for response...";
let apiUrl = 'https://api.openai.com/v1/chat/completions';

let apiKey;
let data;
let updateApiKeyButton;
let userPrompt;

function setup() {
  createCanvas(400, 400);
  apiKeyInput = createInput();
  apiKeyInput.attribute('placeholder', 'API Key');
  apiKeyInput.position(baseMargin, baseMargin);
  apiKeyInput.size(200);
  
  updateApiKeyButton = createButton('Update');
  updateApiKeyButton.position(apiKeyInput.width + updateApiKeyButton.width / 2 + baseMargin / 2, baseMargin);
  updateApiKeyButton.mousePressed(() => {apiKey = apiKeyInput.value(); console.log("API Key has been updated!")});

  let promptInput = createInput();
  promptInput.attribute('placeholder', 'Prompt');
  promptInput.position(baseMargin, baseMargin * 3);
  promptInput.size(200);

  let sendPromptButton = createButton('Prompt');
  sendPromptButton.position(promptInput.width + sendPromptButton.width / 2 + baseMargin / 2, baseMargin * 3);
  sendPromptButton.mousePressed(() => { 
    userPrompt = promptInput.value(); 
    requestData = constructRequestData(userPrompt); 
    apiQuery(apiUrl, apiKey, requestData);
  });
};

function draw() {
  background(255);
  
  push();
  stroke(124, 124, 124);
  rect(baseMargin, baseMargin * 5, width-baseMargin*2, height-baseMargin*6);
  pop();

  textSize(16);
  text(textResponse, baseMargin*2, baseMargin * 6, width - baseMargin*3, height - baseMargin*6);
}

function apiQuery(apiUrl, apiKey, requestData) {
  fetch(apiUrl, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '+ apiKey
    },
    body: JSON.stringify(requestData)
  })
 .then(response => response.json())
 .then(data => {
    console.log("Prompt sent successfully!")
    console.log(data)
    textResponse = data.choices[0].message.content;
  })
 .catch(error => {
    console.error(error);
  });
}

function constructRequestData(prompt, model = "gpt-3.5-turbo") {
  return {
    "model": model,
    "messages": [
      {
        "role": "system",
        "content": "You are a helpful assistant."
      },
      {
        "role": "user",
        "content": prompt
      }
    ]
  };
}