let data;
let textResponse = "Waiting for response...";

function setup() {
  createCanvas(400, 400);
  // This is the API endpoint which enables you to prompt the model to generate a response.
  let apiUrl = 'https://api.openai.com/v1/chat/completions';
  // This is your API key. It is passed when the HTTP requested is made, and it authenticates you. Generated through OpenAIs portal/site.
  let apiKey = 'DIN API NØGLE SKAL VÆRE HER';
  // This is the data that is sent to the API. It is a JSON object that contains the model name, and the messages that are sent to the model.
  // Model is specified as gpt-3.5-turbo (same model as ChatGPT)
  // Messages is an array of objects. First object defines the role of the AI. Second object defines prompt/question asked by the user.
  let requestData = {
    "model": "gpt-3.5-turbo",
    "messages": [
      {
        "role": "system",
        "content": "You are a helpful assistant."
      },
      {
        "role": "user",
        "content": "What is p5.js?"
      }
    ]
  };

  // The API query uses the fetch function, which is a built-in function in JavaScript that makes HTTP requests.
  // It sends a request from the client to the server. Receives a response containing the reply.

  // Method: HTTP verb - Post. It is used to send data to the server. Will receive data too in this case!
  // Headers: It specifies the content type of the data being sent, and the API key that is used to authenticate the request.
  // Body: It is the data that is sent to the API. It is converted to a string using JSON.stringify.

  // The response from the API is then converted to JSON format using the response.json() function.
  // .then chain is used to handle response from api. Think about it as then do this, then do that. Chain reaction.
  // .catch is used to handle errors that occur during the fetch request.
  fetch(apiUrl, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '+ apiKey
    },
    body: JSON.stringify(requestData)
  })
 .then(response => response.json())
 .then(data => {
    textResponse = data.choices[0].message.content;
  })
 .catch(error => {
    console.error(error);
  });
}

function draw() {
  background(255);
  fill(0);
  textSize(16);
  text(textResponse, 20, 20, width - 40, height - 40);
}