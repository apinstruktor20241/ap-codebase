
/*jshint esversion: 9 */
// Specify your API token for authentication.
const API_TOKEN = "YOUR_API_KEY_HERE";
// Specify which model to use through model ID. Larger ones can take a really long time to process.
const MODEL_ID = "gpt2";
// Construct full URL for the API endpoint through base URL and model ID. 
const HF_INFERENCE_URL = "https://api-inference.huggingface.co/models/" + MODEL_ID;
// Specify the prompt that you want to send to the model.
let example_prompt = "What is p5.js?"
let textResponse = "Waiting for response...";

function setup() {
    createCanvas(400, 400);
    // Async/await + fetch is used to query the model, and get the resulting response.
    // In async functions, you can use the await keyword to wait for a promise to resolve.
    // The fetch function is used to make a request to the API endpoint, and get the response.
    // Header defiens the authorization (your api key). Method is POST, as we are sending data to the server.
    // Body is the data that is sent to the API, in our case its the prompt. It is converted to a string using JSON.stringify.
    // The await keyword is used to wait for the response from the API. The response is then converted to JSON format.
    async function query(data) {
        const response = await fetch(
            HF_INFERENCE_URL,
            {
                headers: { Authorization: `Bearer ${API_TOKEN}` },
                method: "POST",
                body: JSON.stringify(data),
            }
        );
        const result = await response.json();
        return result;
    }
    // Here the function is called. The data, which makes up the body, is constituted of the prompt and the max number of tokens. It is passed as an object.
    // textResponse is redefined based on the returned response from the model.
    query({inputs: example_prompt, max_new_tokens: 250}).then((response) => {
        textResponse = response[0].generated_text;
    });
}

function draw(){
    background(255);
    fill(0);
    textSize(16);
    text(textResponse, 20, 20, width - 40, height - 40);
}
