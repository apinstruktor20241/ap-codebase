# Aesthetic Programming F2024 @ Aarhus University
**Teachers**: Germán Leiva (leiva@cc.au.dk), Rikke Hagensby Jensen (rhj@cc.au.dk). <br>
**TA**: Gabriel Høst Andersen (202106188@post.au.dk), Trine Rye Østergaard (troe@cc.au.dk).

## 📒 Book: 
- Soon Winnie & Cox, Geoff, Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PDF here: http://www.openhumanitiespress.org/books/titles/aesthetic-programming/ <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Online here: https://aesthetic-programming.net/ <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Example code from the book here: https://aesthetic-programming.gitlab.io/book/<br>

## 🧑‍🏫 Teaching

### 📖 Lectures

 Mondays 14.00-17.00 @5361-135 <br>
 Wednesdays 11.00-14.00 @5361-144

---

### 🐧 TA sessions

Tuesdays 14.00-16.00 @5361-144 <br>
Fridays 08.00-10.00 @5008-128

## 📝 Brief primer on the course
[Aesthetic Programming](https://kursuskatalog.au.dk/en/course/115561/Aesthetic-programming) provides an introduction to programming while exploring the relationship between code and cultural/aesthetic phenomena in close connection with the field of software studies. In the course, programming will function as a practice from which we can describe and conceptualize the world around us, and thus begin to understand the structures and systems that are in modern digital culture. We will primarily use the programming language p5.js, which forms the basis for future courses in the Digital Design program.

The purpose of the course is for students to learn to design and program a piece of software, not only to understand the pragmatic function of coding, but also to be able to understand programming as a reflexive, aesthetic, and critical act. It is on this basis that students are able to analyze, discuss, and evaluate code.

## 📜 Academic objectives
In the evaluation of the students’ performance, emphasis is placed on the extent to which the students are able to:

### 🧠 Knowledge:
- Demonstrate insight into and reflect on programming as a practice that allows you to explore, think about and understand contemporary digital culture
- Demonstrate insight into programming and programming principles by writing, reading and processing code

### 🛠️ Skills:
- Write, read and process code, including communicating basic programming concepts
- Analyse and asses their own and others’ attempts at expressing aesthetic issues in programme code

### 🎓 Competences:
- Read and write computer code in a given programming language
- Use programming to explore digital culture
- Understand programming as an aesthetic and critical action which extends beyond the practical function of the code

## 💻 miniX - Weekly programming exercises
🔗 [Go to overview over all miniX assignments](../main/p5js/miniX) (located in the p5js/miniX subdirectory)

You will (mostly) every week be asked to submit a programming exercise related to the weekly theme, these are called miniX's (mini exercises).

Deadlines for miniX assignments are sundays after the related week's teaching.

#### 💻 A miniX submission consists of : 
⌨️ **RUNME**: <br>
Source code which should fulfill the following requirements:
- Contain the necessary JS libraries, with working (relative/absolute) links to all files.
- An HTML file (ideally titled index.html for easier links on GitLab)
- A sketch.js file containing your JavaScript (mostly p5.js) code.

🔎 **README**: <br>
Written documentation and reflection of your code which should fulfill the following requirements:
- Working link to your program.
- Working link to your source code / miniX directory
- Screenshot or video of the final product.
- Writing about the reflections experienced through the exercise, the coding of your submission.
- Reflections surrounding the questions posed by the assignment (and the theme).
- References(!)

[🔗 Check out the markdown references for technical guidance](#markdown) <br>
This constitutes a miniX, and should be submitted on Brightspace with links to your GitLab repositry containing your miniX files. Some miniX assignments are individual, while some are group assignments.

#### ✍️ Peer-feedback on miniX
[🔗 Google Sheets overview of Gitlab links and peer feedback schedule](https://docs.google.com/spreadsheets/d/1jtbOM80xrg-WHX1zT1ODXUqtQF2Se68ZAzKg4q2T9HM/edit?usp=sharing)

There's a lot of learning to be had from writing for miniX's, but also from reading, discussing, and reflecting upon each other's code! That's why you should spend some time giving your peers feedback.

When a miniX assignment has been turned in and publicized on GitLab on sundays, peer feedback should be given by 2 other students through GitLab. Deadlines for peer-feedback are wednesdays before class.


#### 🧭 Adding peer-feedback on GitLab
[🔗 Instructional video by Trine](https://brightspace.au.dk/content/enforced/123560-LR28279/Feedback%20-%20Skabt%20med%20Clipchamp_1709570604656.mp4)
1. Go to the person's GitLab repository, find 'Issues' in the navigation pane.
2. Click on the 'New issue' button.
3. Write your feedback with the title "Feedback on miniX[number] by [Your full name]"

## </> Programming environment
- **Programming language**: HTML, CSS & JavaScript - Mainly [p5.js](https://p5js.org/)
- **Code editor**: [Visual Studio Code](https://code.visualstudio.com/). This is where we can open our projects and write our code. The extension _Live Server_ enables us to open a local port, 'previewing' the code live.
- **Development platform**: [GitLab](https://gitlab.com/). This is where we can store our code remotely. Sharing our code with others, and enabling peer feedback.
- **Collaborative coding**: [Live Share](https://code.visualstudio.com/learn/collaboration/live-share) The plugin Live Share for Visual Studio Code enables quick collaborative coding sessions (useful for group work!)

## 💬 Exam 17/06-20/06

🗓️ **NOTE**: The exam dates are subject to change. Please stay updated through [the exam schedule for BA Digital Design](https://studerende.au.dk/studier/fagportaler/arts/eksamen/eksamensplaner/145102-202-302-digital-design)

The exam for the Aesthetic programming course is an oral exam, where the individual student presents, analyzes, and reflects upon the final project and a given miniX assignment (The miniX assignment to be presented is selected by the examiners through randomization).

The exam lasts a total of 30 minutes: The student presents their work for 10 minutes (5 minutes for final project, 5 minutes for miniX). The presentation is followed by 12 minutes of discussion. The exam is concluded by 8 minutes of evaluation.

The presentation of the final project should be guided by the following specification:

> Select a concrete technical example (small code snippet of approximately 5-10 lines) from your final project, and discuss an aesthetic aspect of coding, which informs your critical reflection about digital design / digital culture.

#### 📁 Exam structure
- 5 minute presentation of the student's final project.
- 5 minute presentation of a randomly selected miniX assignment presented during the semester.
- 12 minute discussion between student and examiners.
- 8 minutes for evaluation.

Additionally, you can refer to the following (danish) graphic.

<img src="https://gitlab.com/Margretelr/aestetisk-programmering-2023/-/raw/main/eksamen.png" alt="Image containing an overview of the exam structure" style="border: 3px solid gray;">

#### 🔄 Re-examination
- The re-exam follows the structure of the ordinary exam.
- The deadline for signing up for the re-exam is 23/06-24.
- The re-exams are conducted on the 23/08/24.


## ✨ Inspiration
- [Aesthetic programming book showcase](https://aesthetic-programming.net/pages/showcase.html)
- [Aesthetic programming course showcase 2023](https://gitlab.com/Margretelr/aestetisk-programmering-2023/-/blob/main/Showcase.md?ref_type=heads)
- [The coding train challenges](https://thecodingtrain.com/challenges)
- [Genuary 2024](https://genuary.art/)
- [Gabriel's GitLab](https://gitlab.com/9plus10savage/aesthetic-programming)
- [Trine's GitLab](https://gitlab.com/TrineRye/aestheticprogramming)

## 📚 Resources

### 🌐 HTML | CSS
- [A Complete Guide to CSS Flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)
- [A Complete Guide to CSS Grid](https://css-tricks.com/snippets/css/complete-guide-grid/)
- [freeCodeCamp Responsive Web Design course](https://www.freecodecamp.org/learn/2022/responsive-web-design/)
- [Udacity HTML CSS Course](https://www.udacity.com/course/intro-to-html-and-css--ud001)
- [CSS Basics](https://www.youtube.com/watch?v=C5zTzUFXQrE)

### ♨️ JavaScript
- [JavaScript basics](https://github.com/processing/p5.js/wiki/JavaScript-basics) by p5.js
- [The modern JavaScript tutorial](https://javascript.info/)

### 🟪 p5js
- [p5.js reference](https://p5js.org/reference/)
- [freeCodeCamp p5.js course](https://www.freecodecamp.org/news/art-of-coding-with-p5js/)
- [p5.js Tutorials by Daniel Shiffman](https://www.youtube.com/playlist?list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA)
- [Intro to Programming in p5.js by Xin Xin](https://www.youtube.com/watch?v=wiG7wLwyW0E&list=PLT233rQkMw761t_nQ_6GkejNT1g3Ew4PU)

### 🤖 AI
<!--- If you guys have any good resources let me know, was mainly thinking of adding something to help the students utilize AI as an aid, rather than a solution --->
- [Learn How to Use AI for Coding](https://www.codecademy.com/learn/prompt-engineering-for-software-engineers)
- [Prompt Engineering Guide](https://learnprompting.org/docs/intro)
- [OpenAI Prompt engineering](https://platform.openai.com/docs/guides/prompt-engineering)


### 📃 Markdown
- [Markdown guide](https://www.markdownguide.org/getting-started/)
- [Markdown cheat sheet](https://www.markdownguide.org/cheat-sheet/)
- [Stackedit (browser-based editor)](https://stackedit.io/)
- [Markdown All in One (VS code plugin)](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one)
- [Obisidan (PKM & Note-taking in MD)](https://obsidian.md/)

### ⚡ Git | GitHub | GitLab
- [Software Carpentries course on Git](https://swcarpentry.github.io/git-novice/index.html)
- [Git cheat sheet from Github Education](https://education.github.com/git-cheat-sheet-education.pdf)
- [GitHub Student Developer Pack](https://education.github.com/pack)

### 📝 Academic Reading & Writing
- [Zotero](https://www.zotero.org/)